//khai báo thư viện 
const express = require('express');
const { getAllCar, getAnCarById, createCar, updateCarById, deleteCarByID, getAllCarOfUser } = require('../controllers/carController');

//khai báo một biến ROuter chạy trên file index.js
const carRouter = express.Router();

carRouter.get('/cars/',getAllCar);
//get all cars of user
carRouter.get('/cars/users/:userId',getAllCarOfUser);

carRouter.get('/cars/:carId',getAnCarById);

carRouter.post('/cars/users/:userId',createCar);
// carRouter.post('/cars/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })

carRouter.put('/cars/:carId',updateCarById);

carRouter.delete('/cars/:carId/users/:userId',deleteCarByID);

module.exports = {
    carRouter
}