// Import thư viện mongoose
const { request } = require("http");
const mongoose = require("mongoose");

// Import cars Model
const carModel = require('../models/carModel');
const userModel = require('../models/userModel');
const getAllCar = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    carModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all Car',
         product: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAnCarById = (request,response) => {
    //b1: thu thập dữ liệu
    const carId = request.params.carId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(carId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${carId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    carModel.findById(carId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             productType: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const createCar = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   const userId = request.params.userId;
    
    // B2: Validate dữ liệu
    // 
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
           status: 'Bad Request',
           message: `${userId} ko hợp lệ`
        })
     }

    if(!body.model) {
        return response.status(400).json({
            status: "Bad Request",
            message: "name không hợp lệ"
        })
    }


    if(!(body.vld) ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "vld không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newcar = {
       
        model: body.model,
        vld: body.vld,
       
    }

    carModel.create(newcar)
     .then((data) => {
        userModel.findByIdAndUpdate(userId,{$push:{cars:data._id}})
        .then((data1) => {
            response.status(201).json({
            message: 'Successful to create an Car and push in  user',
            user: data1
            })
        console.log(userId);
        console.log(data._id);
         })
        .catch((error)=> {
            response.status(500).json({
                message: `internal sever ${error}`
            })
        })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updateCarById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const carId = request.params.carId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(carId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if(body.vld !== undefined && body.vld.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "vld không hợp lệ"
        })
    }

   

    if(body.model !== undefined && body.model.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "model không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updatecar = {
        model: body.model,
        vld: body.vld,
       
    }

    

    carModel.findByIdAndUpdate(carId,updatecar)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new car',
             drink: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deleteCarByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const carId = request.params.carId;
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "carId không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    carModel.findByIdAndDelete(carId)
    .then((data) => {
        userModel.findByIdAndUpdate(userId,{$pull:{cars:data._id}})
        .then((data1) => {
            response.status(201).json({
            message: 'Successful to delete an Car and pull out of  user',
            user: data1
            })
        console.log(userId);
        console.log(data._id);
         })
        .catch((error)=> {
            response.status(500).json({
                message: `internal sever ${error}`
            })
        })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}
 
const getAllCarOfUser = (request,response) => {
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "carId không hợp lệ"
        })
    }

    userModel.findById(userId).populate("cars")
    .then((data) => {
        response.status(201).json({
           message: 'Successful to populate',
           drink: data
        })
     })
     .catch((error) => {
        response.status(500).json({
        message: `internal sever ${error}`
        })
     })
}
module.exports = {
    deleteCarByID,
    updateCarById,
    createCar,
    getAnCarById,
    getAllCar,
    getAllCarOfUser
}