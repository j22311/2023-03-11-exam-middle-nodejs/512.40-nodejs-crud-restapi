// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Users Model
const userModel = require('../models/userModel');

const getAllUser = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    userModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all user',
         product: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAnUserById = (request,response) => {
    //b1: thu thập dữ liệu
    const userId = request.params.userId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${userId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    userModel.findById(userId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             productType: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const createUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    if(!body.name) {
        return response.status(400).json({
            status: "Bad Request",
            message: "name không hợp lệ"
        })
    }

    // Kiểm tra noStudent có hợp lệ hay không
    if(isNaN(body.age) || body.age < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "age  không hợp lệ"
        })
    }

    if(!(body.phone) ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "phone không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newUser = {
       
        name: body.name,
        phone: body.phone,
        age: body.age
    }

    userModel.create(newUser)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new newUser',
             productType: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updateUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if(body.name !== undefined && body.name.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "name không hợp lệ"
        })
    }

    if(body.age !== undefined && ( isNaN(body.age) || body.agge < 0 )) {
        return response.status(400).json({
            status: "Bad Request",
            message: "age không hợp lệ"
        })
    }

    if(body.phone !== undefined && body.phone.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "phone không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateUser = {
        name: body.name,
        phone: body.phone,
        age: body.age
    }

    

    userModel.findByIdAndUpdate(userId,updateUser)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new user',
             drink: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deleteUserByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findByIdAndDelete(userId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete an user ',
          productType: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}

module.exports = {
    deleteUserByID,
    updateUserById,
    createUser,
    getAnUserById,
    getAllUser
}