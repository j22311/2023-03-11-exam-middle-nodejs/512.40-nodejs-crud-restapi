const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const carSchema = new Schema({
   model: {
        type: String,
        required: true,
       
    },
    vld: {
        type: String,
        required: false,
        unique: true
    },
    
    
}, {
    timestamps: true
})

module.exports = mongoose.model("Cars", carSchema);