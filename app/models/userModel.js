// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const userSchema = new Schema({
   name: {
        type: String,
        required: true,
       
    },
    phone: {
        type: String,
        required: false,
        unique: true
    },
    age: {
        type: Number,
        default: 0
    },
    // Một course có nhiều review
    cars: [{
        type: mongoose.Types.ObjectId,
        ref: "Cars"
    }]
    
}, {
    timestamps: true
})

module.exports = mongoose.model("Users", userSchema);