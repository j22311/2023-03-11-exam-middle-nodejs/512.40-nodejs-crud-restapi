///khai báo thưu viện Express
const express = require('express');

//-----b1khai báo thư viện mongoose-----
var mongoose = require('mongoose');
const { carRouter } = require('./app/routes/carRouter');

const { userRouter } = require('./app/routes/userRouter');

//khai báo app để chạy
const app = express();

// khai báo port để chạy trên post man 
const port = 8000;

// -----khỡi động app qua cổng------
app.listen(port, () => {
    console.log(`app listen on port ${port}`);
}) 

// Cấu hình request đọc được body json
app.use(express.json());

//hàm này chạy ghi ra ngày tháng trong terminal
app.use((req,res,next) => {
    console.log(new Date());

    next();
}) 

// kết nối với mongoosebd
main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/CRUD_User');
  console.log('Successfully connected mongoDB');
  
}

// //khai báo app chay trên userRouter
app.use('/',userRouter);
// //khai báo app chay trên carRouter
app.use('/',carRouter);